{-# OPTIONS_GHC -Wall #-}

-- CIS 194: Introduction to Haskell (Homework 1)

-- Exercise 1
toDigits :: Integer -> [Integer]
toDigitsRev :: Integer -> [Integer]

toDigits 0 = []
toDigits n = toDigits (n `div` 10) ++ [n `mod` 10]

toDigitsRev n = reverse $ toDigits n

-- Exercise 2 (ick)
doubleEveryOther :: [Integer] -> [Integer]
doubleEveryOther [] = []
--doubleEveryOther n =  
